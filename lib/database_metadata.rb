# frozen_string_literal: true

require 'net/http'
require 'zip'
require 'yaml'
require 'active_support/all'

module DatabaseMetadata
  extend self

  METADATA_ZIP = 'https://gitlab.com/gitlab-org/gitlab/-/archive/master/gitlab-master.zip?path=db/docs'
  CATEGORIES_YML = 'https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/categories.yml?inline=false'

  def refresh_local!
    path = File.join(__dir__, '..', 'tmp', 'database_metadata')
    FileUtils.rm_r(path) if File.directory?(path)
    FileUtils.mkdir_p(path)

    zip = File.join(path, 'metadata.zip')
    File.write(zip, Net::HTTP.get(URI.parse(METADATA_ZIP)))

    Zip::File.open(zip) do |zipfile|
      zipfile.each do |file|
        fpath = File.join(path, file.name)
        zipfile.extract(file, fpath) unless File.exist?(fpath)
      end
    end

    Dir.glob(File.join(path, 'gitlab-master-db-docs/db/docs/*.yml')).each do |file|
      table = YAML.load_file(file)
      tables[table['table_name']] = table.symbolize_keys
    end

    ## Update categories.yml
    catfile = File.join(path, 'categories.yml')
    File.write(catfile, Net::HTTP.get(URI.parse(CATEGORIES_YML)))

    YAML.load_file(catfile).each do |category, attrs|
      categories[category] = attrs.symbolize_keys
    end
  end

  def for_table(name)
    tables[name]
  end

  def for_category(name)
    categories[name]
  end

  def stage_groups_for_table(table_name)
    unassigned = ['unassigned']

    table_metadata = for_table(table_name)
    return unassigned unless table_metadata

    stage_groups = table_metadata[:feature_categories].map do |cat|
      DatabaseMetadata.for_category(cat)
    end

    stage_groups.reject(&:nil?).map { |cat| cat[:stage] } || unassigned
  end

  def categories
    @categories ||= {}
  end

  def tables
    @tables ||= {}
  end
end
