# frozen_string_literal: true

Index = Struct.new(:schema, :name, :tablename, :ondisk_size_bytes, :definition) do
  def to_s
    "#{schema}.#{name}"
  end

  def unique?
    definition&.downcase =~ /^create unique/
  end
end
