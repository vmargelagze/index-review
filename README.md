# Index review

This repo contains tooling to review existing indexes.

## Check for unused indexes

This is based on production data and allows us to detect which indexes have not been used actively in the past. We use an export of index information from production and combine this with prometheus data. An index is detected as "unused" if it has not seen any activity (reads) within the last 7 days.

The tool generates overview and per-group reports, so those indexes can be further reviewed.

### Setup

1. Setup port-forwarding for prometheus through a bastion host (when run locally): `ssh -L 10902:thanos-query-frontend-internal.ops.gke.gitlab.net:9090 lb-bastion.gstg.gitlab.com`
1. Dump index information from a production host or dblab: `\copy (select schema, name, tablename, ondisk_size_bytes from postgres_indexes order by identifier) TO 'data/indexes.csv' WITH csv`
1. Retrieve data from prometheus: `bundle exec ruby retrieve-unused-indexes.rb`
1. Generate reports `bundle exec ruby generate-reports.rb`

The results will be written into CSV and Markdown files in `data/`.
