# frozen_string_literal: true

require 'pry'
require 'csv'
require 'erb'
require 'filesize'
require_relative 'lib/database_metadata'
require_relative 'lib/index'

DatabaseMetadata.refresh_local!

all_indexes = CSV.read('data/unused-indexes.csv', headers: true).each_with_object({}) do |row, h|
  index = Index.new(row['schema'], row['name'], row['tablename'], Integer(row['ondisk_size_bytes']), row['definition'])

  h[index] = {
    activity: row['activity'] == 'true',
    stage_groups: DatabaseMetadata.stage_groups_for_table(row['tablename'])
  }
end

candidates = all_indexes.reject do |index, attrs|
  index.unique? || attrs[:activity] # discard unique or actively used indexes
end

by_group = candidates.each_with_object({}) do |(index, attrs), h|
  attrs[:stage_groups].each do |group|
    h[group] ||= []
    h[group] << index
  end
end

CSV.open('data/unused-indexes-per-group.csv', 'wb') do |csv|
  csv << %i[name tablename ondisk_size_bytes stage_group]

  by_group.each do |group, indexes|
    indexes.each do |index, _|
      csv << [index.name, index.tablename, index.ondisk_size_bytes, group]
    end
  end
end

CSV.open('data/unused-indexes-all.csv', 'wb') do |csv|
  csv << %i[name tablename ondisk_size_bytes]

  candidates.each do |index, _|
    csv << [index.name, index.tablename, index.ondisk_size_bytes]
  end
end

def format_bytes(bytes)
  Filesize.from("#{bytes} B").pretty
end

File.open('data/unused-indexes-per-group.md', 'wb+') do |io|
  group_summaries = []

  by_group.each do |group, indexes|
    erb = ERB.new(File.read('templates/stage_group_summary.erb'), 0, '<>')
    group_summaries << erb.result(binding)
  end

  io << ERB.new(File.read('templates/summary.erb'), 0, '<>').result(binding)
end

exit 0