# frozen_string_literal: true

require 'csv'
require 'prometheus/api_client'
require 'pry'
require_relative 'lib/index'

SECONDS_OF_INACTIVITY = 7 * 24 * 3600 # 7 days

# \copy (select schema, name, tablename, ondisk_size_bytes, definition from postgres_indexes order by identifier) TO 'indexes.csv' WITH csv;
indexes = CSV.read('data/indexes.csv').map { |(s, n, t, o, d)| Index.new(s, n, t, Integer(o), d) }.sort_by(&:ondisk_size_bytes).reverse

# ssh -L 10902:thanos-query-frontend-internal.ops.gke.gitlab.net:9090 lb-bastion.gstg.gitlab.com
options = { timeout: 120 }
prometheus_url = ENV['PROMETHEUS_URL'] || 'http://localhost:10902'
prometheus = Prometheus::ApiClient.client(url: prometheus_url, options: options)

puts "Using prometheus: #{prometheus_url}"

def for_index(index)
  params = { env: 'gprd', schemaname: index.schema, indexrelname: index.name }
  params.map { |k, v| "#{k}=\"#{v}\"" }.join(', ')
end

def activity?(prom_result)
  prom_result['result'].first['values'].any? { |(_, v)| v != '0' }
end

CSV.open('data/unused-indexes.csv', 'wb') do |csv|
  index_attributes = %i[schema name tablename ondisk_size_bytes definition]
  # write header
  csv << index_attributes + %i[unique activity]

  unused = indexes.each_with_object({}) do |index, result|
    query = <<~PROMQL
      sum(rate(pg_stat_user_indexes_idx_scan{#{for_index(index)}}[5m]))
      + sum(rate(pg_stat_user_indexes_idx_tup_fetch{#{for_index(index)}}[5m]))
      + sum(rate(pg_stat_user_indexes_idx_tup_read{#{for_index(index)}}[5m]))
    PROMQL

    data = prometheus.query_range(
      query: query,
      start: (Time.now.utc - SECONDS_OF_INACTIVITY).strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
      end: Time.now.utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
      step: '120s'
    )

    result[index] = { activity: activity?(data) }

    puts "Index #{index}: #{result[index]}"
    csv << [index.to_h.slice(*index_attributes).values, result[index][:unique], result[index][:activity]].flatten
    csv.flush
  rescue => e
    puts "Error when processing #{index}: #{e}"
  end

  number_of_unused = unused.reject { |_, attrs| attrs[:activity] }.count
  puts "Found #{number_of_unused} indexes"
end
